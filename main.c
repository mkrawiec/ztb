#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>

#define MAX 99999

typedef struct {
    int id;
    char name[20];
    char desc[90];
} Rec;

struct rusage mem;

int main(int argc, char *argv[]) {
    Rec *data =malloc(sizeof(Rec)*MAX);
    clock_t c1,c2;

    if (data==NULL) {
        fprintf(stderr,"Mem alloc error\n");
        return 1;
    }

    c1=clock();

    for(int i = 0; i < MAX; i++) {
        Rec el = { .id = i, .name = "test", .desc = "test123" };
        data[i] = el;
    }
    c2=clock();
    printf("time, c2-c1[s]: %f\n", ((float)c2-(float)c1)/CLOCKS_PER_SEC);

    free(data);

    getrusage(RUSAGE_SELF,&mem);
    printf("Max mem usage[kB]: %ld\n", mem.ru_maxrss);
    return 0;
}
