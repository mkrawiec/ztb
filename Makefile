CC=gcc -std=c11
CFLAGS=-Wall

all: main
main: main.o
main.o: main.c

clean:
	rm -f main main.o

run: main
	./main
